<?php
/*
Plugin Name: City
Description: Filter regions and cities
Version: 1.0
Author: Aharon Yedigaryan
*/

function city_menu() {
	add_menu_page( 'City Options', 'City', 'manage_options', 'city-unique-identifier', 'city_options' );
}

function city_options() {
	
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	require_once('admin.php');
}

add_action( 'admin_menu', 'city_menu' );

function html_form_code() {
	require_once('view.php');
}

function deliver_mail() {

	// if the submit button is clicked, send the email
	if ( isset( $_POST['cf-submitted'] ) ) {

		// sanitize form values
		$name    = sanitize_text_field( $_POST["cf-name"] );
		$email   = sanitize_email( $_POST["cf-email"] );
		$subject = sanitize_text_field( $_POST["cf-subject"] );
		$message = esc_textarea( $_POST["cf-message"] );

		// get the blog administrator's email address
		$to = get_option( 'admin_email' );

		$headers = "From: $name <$email>" . "\r\n";

		// If email has been process for sending, display a success message
		if ( wp_mail( $to, $subject, $message, $headers ) ) {
			echo '<div>';
			echo '<p>Thanks for contacting me, expect a response soon.</p>';
			echo '</div>';
		} else {
			echo 'An unexpected error occurred';
		}
	}
}

function city_options_install() {
   	$your_db_name = $wpdb->prefix . 'city';
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	// create the ECPT metabox database table
	if($wpdb->get_var("show tables like '".$your_db_name."'") != $your_db_name) 
	{
		$sql = "CREATE TABLE " . $your_db_name . " (
		`id` mediumint(9) NOT NULL AUTO_INCREMENT,
		`city` tinytext NOT NULL,
		`type` int(11) NOT NULL,
		`relation` int(11) NOT NULL,
		UNIQUE KEY id (id)
		);";
 
		
		dbDelta($sql);
	}
 
}
// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'city_options_install');

function city_shortcode() {
	ob_start();
	deliver_mail();
	html_form_code();

	return ob_get_clean();
}

add_shortcode( 'city_filter', 'city_shortcode' );

?>