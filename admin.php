<?php
global $wpdb;

$sql_r = "SELECT * FROM city WHERE type='1'";
$results_r = $wpdb->get_results($sql_r);
$region_count_r = $wpdb->num_rows;

$sql_c = "SELECT * FROM city WHERE type='2'";
$results_c = $wpdb->get_results($sql_c);
$region_count_c = $wpdb->num_rows;

?>


<input type="text" id="region" name="region"><input type="button" value="Add Region" onclick="add_region(this);"><br>


<?php 

	if($region_count_r>0){

?>
	<input type="text" name="city" id="city"> 
	<select class="region" id="city_rel">
	<option>Select Region</option>
	
<?php
	foreach( $results_r as $result ) {
?>
    <option value="<?php echo $result->id; ?>"><?php echo $result->city; ?></option>
<?php
    }
?>
</select>
<input type="button" value="Add City" onclick="add_city();">
<br> 
<?php
	if($region_count_c>0){
?>	
	<input type="text" name="village" id="village"> 

<select class="city" id="village_rel">
	<option>Select City</option>
<?php
	foreach( $results_c as $result ) {
?>
    <option value="<?php echo $result->id; ?>"><?php echo $result->city; ?></option>
<?php
    }
?>
</select>
<input type="button" value="Add Village" onclick="add_village();">
<br> 	

<?php	
	}
	else
		echo "Please Add City";
?>


		
<?php 
	
	}
	else
		echo "Please Add Region";

?>

<script src="<?php echo plugins_url( 'city/js/jquery.js', dirname(__FILE__) ) ?>"></script>
<script>
	function add_region()
	{
	   $.ajax({
		 type: 'post',
		 url: '../wp-content/plugins/city/filter.php',
		 data: {
		   set_region:$('#region').val()
		 },
		 success: function (response) {
		   $(".region").append(response); 
		   $("#region").val("");
		   location.reload();
		 }
	   });
	}
	
	function add_city()
	{
	   $.ajax({
		 type: 'post',
		 url: '../wp-content/plugins/city/filter.php',
		 data: {
		   set_city:$('#city').val(),
		   set_city_r:$('#city_rel').val()
		 },
		 success: function (response) {
		   $(".city").append(response); 
		   $("#city").val("");
		   location.reload();
		 }
	   });
	}
	
	function add_village()
	{
		$.ajax({
		 type: 'post',
		 url: '../wp-content/plugins/city/filter.php',
		 data: {
		   set_village:$('#village').val(),
		   set_village_r:$('#village_rel').val()
		 },
		 success: function (response) {
			$("#village").val("");
		 }
	   });
	}
</script>