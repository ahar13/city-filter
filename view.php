<?php
	
global $wpdb;
$sql = "SELECT * FROM city WHERE type='1'";
$results = $wpdb->get_results($sql);
 if($wpdb->num_rows>0)
 {
?>
<div style="width:700px;">
<select id="region" onchange="select_region(this.value);">
	<option value="regval">Please Select Region</option>
<?php
	foreach( $results as $result ) {
?>
    <option value="<?php echo $result->id; ?>"><?php echo $result->city; ?></option>
<?php
    }
?>
</select>

<select id="city" onchange="select_city(this.value);">
	<option value="cityval">Not Selected Region</option>
</select>

<select id="village" >
	<option value="vilval">Not Selected City</option>
</select>

</div>

<script src="<?php echo plugins_url( 'city/js/jquery.js', dirname(__FILE__) ) ?>"></script>
<script>
	function select_region(val)
	{
	   $.ajax({
		 type: 'post',
		 url: '../wp-content/plugins/city/filter.php',
		 data: {
		   get_city:val
		 },
		 success: function (response) {
			 if(response != "regval")
			 {
				document.getElementById("city").innerHTML=response;
			 }
		 }
	   });
	}
	
	function select_city(val)
	{
	   $.ajax({
		 type: 'post',
		 url: '../wp-content/plugins/city/filter.php',
		 data: {
		   get_village:val
		 },
		 success: function (response) {
			if(response != "cityval")
			{
				document.getElementById("village").innerHTML=response;
			} 
		 }
	   });
	}
</script>
<?php
 }
 else
	 echo "Region List is Empty";
 ?>
 